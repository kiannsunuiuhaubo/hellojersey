/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.hellojersey;

/**
 *
 * @author 70609-A
 */
public class HelloBean {
    private String helloString = null;
    private long time=-1;
    public String getHelloString(){
    return helloString;
    }
    public void setHelloString(String helloString){
    this.helloString = helloString;
    }
    public long getTime(){
    return time;
    }
    public void setTime(long time){
    this.time=time;
    }

}
