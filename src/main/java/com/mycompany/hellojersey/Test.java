/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.hellojersey;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;




/**
 *
 * @author 70609-A
 */
@Path("test")
public class Test {
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("name/{u}")

    public HelloBean sayHello(@PathParam("u") String name){
    HelloBean b = new HelloBean();
    b.setHelloString("Hello, "+name);
    b.setTime(System.currentTimeMillis());
    return b;
    }
}